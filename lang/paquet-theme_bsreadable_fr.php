<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bsreadable_description' => 'Optimized for legibility',
	'theme_bsreadable_slogan' => 'Optimized for legibility',
);
